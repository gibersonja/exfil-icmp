package main

import (
	"bufio"
	"encoding/binary"
	"fmt"
	"io"
	"log"
	"net"
	"os"
	"path"
	"runtime"
	"strconv"
	"time"
)

var err error
var data []byte
var dest string
var size int = 32
var sequence uint16
var server bool = false
var ident [2]byte
var retransmit int = 3
var bufferWait int = 1   // wait between packets (ms)
var finalWait int = 3000 // wait to send final packet (ms)

func main() {
	ident[0] = 0b101
	ident[1] = 0b00111001

	if isPipe() {
		nBytes, nChunks := int64(0), int64(0)
		r := bufio.NewReader(os.Stdin)
		buf := make([]byte, 0, 4*1024)
		for {
			n, err := r.Read(buf[:cap(buf)])
			buf = buf[:n]
			if n == 0 {
				if err == nil {
					continue
				}
				if err == io.EOF {
					break
				}
				er(err)
			}
			nChunks++
			nBytes += int64(len(buf))
			data = append(data, buf...)

			if err != nil && err != io.EOF {
				er(err)
			}
		}
	}

	args := os.Args[1:]

	for n := 0; n < len(args); n++ {
		arg := args[n]

		if arg == "--help" || arg == "-h" {
			fmt.Print("-h | --help\t\tPrint this help message\n")
			fmt.Print("-f | --file\t\tFile to send\n")
			fmt.Print("-s <int> | --size <int>\tData payload size (in bytes) to send (Default: 32, Min: 6)\n")
			fmt.Print("-d <IP | FQDN>\t\tDestination server\n")
			fmt.Print("--server\t\tEnable server mode to recieve files (Default: Client)\n")
			return
		}

		if arg == "--server" {
			server = true
		}

		if (arg == "-s" || arg == "--size") && n+1 < len(args) {
			size, err = strconv.Atoi(args[n+1])
			er(err)

			if size < 6 {
				er(fmt.Errorf("size must be at least 6 bytes"))
			}
		}

		if (arg == "-f" || arg == "--file") && !isPipe() && n+1 < len(args) {
			filename := args[n+1]
			_, err = os.Stat(filename)
			er(err)

			fh, err := os.Open(filename)
			er(err)
			defer fh.Close()

			b := make([]byte, 1)

			for {
				_, err := fh.Read(b)
				if err == io.EOF {
					break
				} else {
					er(err)
				}

				data = append(data, b[0])
			}
		}

		if arg == "-d" && n+1 < len(args) {
			dest = args[n+1]
		}

	}

	if dest == "" {
		er(fmt.Errorf("must provide destination"))
	}

	var typ byte
	var code byte
	var cksum [2]byte

	if server {
		output := recvMsg()
		for i := 0; i < len(output); i++ {
			binary.Write(os.Stdout, binary.LittleEndian, output[i])
		}
	} else {
		var maxSize uint64 = 65536 * uint64(size)
		var sizeFmt string
		var maxSizeFmt string
		var dataFmt string

		if uint64(len(data)) > maxSize {
			if size >= 1024 {
				sizeFmt = fmt.Sprintf(" [%dKB]", size/1024)
			}
			if size >= 1024*1024 {
				sizeFmt = fmt.Sprintf(" [%dMB]", size/(1024*1024))
			}
			if size >= 1024*1024*1024 {
				sizeFmt = fmt.Sprintf(" [%dGB]", size/(1024*1024*1024))
			}

			if maxSize >= 1024 {
				maxSizeFmt = fmt.Sprintf(" [%dKB]", maxSize/1024)
			}
			if maxSize >= 1024*1024 {
				maxSizeFmt = fmt.Sprintf(" [%dMB]", maxSize/(1024*1024))
			}
			if maxSize >= 1024*1024*1024 {
				maxSizeFmt = fmt.Sprintf(" [%dGB]", maxSize/(1024*1024*1024))
			}
			if maxSize >= 1024*1024*1024*1024 {
				maxSizeFmt = fmt.Sprintf(" [%dTB]", maxSize/(1024*1024*1024*1024))
			}

			if len(data) >= 1024 {
				dataFmt = fmt.Sprintf(" [%dKB]", len(data)/1024)
			}
			if len(data) >= 1024*1024 {
				dataFmt = fmt.Sprintf(" [%dMB]", len(data)/(1024*1024))
			}
			if len(data) >= 1024*1024*1024 {
				dataFmt = fmt.Sprintf(" [%dGB]", len(data)/(1024*1024*1024))
			}
			if len(data) >= 1024*1024*1024*1024 {
				dataFmt = fmt.Sprintf(" [%dTB]", len(data)/(1024*1024*1024*1024))
			}

			er(fmt.Errorf("Maximum number of packets before sequence number rollover is 65536B [64KB] (uint16), at payload size of %dB%s yields a possible total size of %dB%s.  Data payload being sent is %dB%s", size, sizeFmt, maxSize, maxSizeFmt, len(data), dataFmt))
		}

		typ = 8

		for j := 0; j < len(data); j = j + size {
			var msg []byte
			for i := 0; i < size; i++ {
				if j+i < len(data) {
					msg = append(msg, data[i+j])
				}
			}
			tmpSeq := uint162bytes(sequence)

			var packet []byte
			packet = append(packet, typ)
			packet = append(packet, code)
			packet = append(packet, 0)
			packet = append(packet, 0)
			packet = append(packet, ident[0])
			packet = append(packet, ident[1])
			packet = append(packet, tmpSeq[0])
			packet = append(packet, tmpSeq[1])
			packet = append(packet, msg...)

			cksum = checksum(packet)

			packet[2] = cksum[0]
			packet[3] = cksum[1]

			for k := 0; k < retransmit; k++ {
				sendMsg(packet, dest)
			}

			sequence += 1

		}

		tmpSeq := uint162bytes(sequence)

		var packet []byte
		packet = append(packet, typ)
		packet = append(packet, code)
		packet = append(packet, 0)
		packet = append(packet, 0)
		packet = append(packet, ident[0])
		packet = append(packet, ident[1])
		packet = append(packet, tmpSeq[0])
		packet = append(packet, tmpSeq[1])
		packet = append(packet, ident[0])
		packet = append(packet, ident[1])
		packet = append(packet, ident[0])
		packet = append(packet, ident[1])
		packet = append(packet, ident[0])
		packet = append(packet, ident[1])

		cksum = checksum(packet)

		packet[2] = cksum[0]
		packet[3] = cksum[1]

		time.Sleep(time.Duration(finalWait) * time.Millisecond)

		for k := 0; k < retransmit; k++ {
			sendMsg(packet, dest)
		}
	}
}

func uint162bytes(input uint16) [2]byte {
	var output [2]byte
	output[0] = byte(input >> 8)
	output[1] = byte(input)
	return output
}

func sendMsg(msg []byte, ip string) {
	time.Sleep(time.Duration(bufferWait) * time.Millisecond)
	conn, err := net.Dial("ip4:1", ip)
	er(err)
	defer conn.Close()

	_, err = conn.Write(msg)
	er(err)

}

func recvMsg() []byte {

	var netIP net.IPAddr
	netIP.IP = net.ParseIP(dest)

	conn, err := net.ListenIP("ip4:icmp", &netIP)
	er(err)

	buf := make([]byte, 4*1024)
	var output []byte

	store := make(map[int][]byte)

	for {
		packetLength, _, err := conn.ReadFrom(buf)
		er(err)

		if buf[4] == ident[0] && buf[5] == ident[1] && 8 <= packetLength {
			seq1 := buf[6]
			seq2 := buf[7]
			var seqNum int = (int(seq1) << 8) + int(seq2)

			if buf[8] == ident[0] && buf[9] == ident[1] {
				for j := 0; j < len(store); j++ {
					for k := 0; k < len(store[j]); k++ {
						output = append(output, store[j][k])
					}
				}

				return output
			}

			_, ok := store[seqNum]
			if !ok {
				for i := 8; i < packetLength; i++ {
					store[seqNum] = append(store[seqNum], buf[i])
				}
			}
		}
	}
}

func checksum(msg []byte) [2]byte {
	var tmp uint32

	if len(msg)%2 != 0 {
		msg = append(msg, 0)
	}

	for i := 0; i < len(msg); i++ {
		if i%2 != 0 {
			tmp += (uint32(msg[i-1]))<<8 + uint32(msg[i])
			tmp = (tmp & 0xFFFF) + (tmp >> 16)
		}
	}

	var cksum [2]byte
	cksum[0] = byte(tmp >> 8)
	cksum[1] = byte(tmp << 8 >> 8)
	cksum[0] = cksum[0] ^ 0xFF
	cksum[1] = cksum[1] ^ 0xFF

	return cksum
}

func er(err error) {
	pc, file, line, ok := runtime.Caller(1)
	if !ok {
		log.Fatal("Error getting caller function\n")
		os.Exit(3)
	}

	if err != nil {
		log.Fatalf("| %v | %s:%d | Error: %v\n", runtime.FuncForPC(pc).Name(),
			path.Base(file), line, err)
	}
}

func isPipe() bool {
	fileInfo, _ := os.Stdin.Stat()
	return fileInfo.Mode()&os.ModeCharDevice == 0
}
